# Flask Security

## What is it

The Quick Start example for Flask-Security

## Installation

To download this code run the command:

    git clone https://rabramley@bitbucket.org/rabramley/flask-trials-tutorials-and-techniques.git

Then cd into the project directory and create a virtual environment for it.  Don't
worry, if you name it the same as me git will ignore it.

    virtualenv venv

Activate the virtual environment

    . venv/bin/activate

Then install Flask

     pip install Flask

Install flask security and SqlAlchemy

    pip install flask-security flask-sqlalchemy

The email settings will not work because the username and password are incorrect.
**DO NOT** edit the username and password in the default settings file as this may get
checked into source control.  Instead, create a file called `settings.py` in the same
directory as `default_settings.py` and add correct the username and password config values in there.

## Running the damn thing

To run the application in debug mode, run...

    python debug.py

then head to [http://127.0.0.1:5000](http://127.0.0.1:5000)
