# configuration
DEBUG = True
SECRET_KEY = 'Something Secret'

# SqlAlchemy
SQLALCHEMY_DATABASE_URI = 'sqlite://'

# Security
SECURITY_REGISTERABLE = True
SECURITY_CONFIRMABLE = True
SECURITY_RECOVERABLE = True
SECURITY_CHANGEABLE = True

# Mail
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'email address'
MAIL_PASSWORD = 'password'
