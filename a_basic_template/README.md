# A Basic Template

## What is it

A basic template from which I can create little test apps to trial stuff.

## Installation

To download this code run the command:

    git clone https://rabramley@bitbucket.org/rabramley/flask-trials-tutorials-and-techniques.git

Then cd into the project directory and create a virtual environment for it.  Don't
worry, if you name it the same as me git will ignore it.

    virtualenv venv

Activate the virtual environment

    . venv/bin/activate

Then install Flask

     pip install Flask

## Running the damn thing

To run the application in debug mode, run...

    python debug.py

then head to [http://127.0.0.1:5000](http://127.0.0.1:5000)
