from flask import Flask
import default_settings

app = Flask(__name__)
app.config.from_object('a_basic_template.default_settings')
app.config.from_envvar('A_BASIC_TEMPLATE_SETTINGS', silent=True)

from a_basic_template.views import *
