from flask import render_template
from a_basic_template import app

@app.route('/')
def home():
    return render_template('home.html')